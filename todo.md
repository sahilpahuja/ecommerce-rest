## Category Relationships
- Category -> Product
- Category -> Seller
- Category -> Transaction
- Category -> Buyer

## Seller Relationships
- Seller -> Product

## Product Relationships
- Product -> Transaction
- Product -> Buyer
- Product -> Category

## Features
- Images in Products
- Email Verification of User
