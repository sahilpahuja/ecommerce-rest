<?php

namespace App\Policies;

use App\Models\Buyer;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TransactionPolicy
{
    use HandlesAuthorization;

    /**
     * This will run before any other policy method if returned from here,
     * then there won't be further method calls.
     *
     * If user is admin, everything will be bypassed
     *
     * @param User $user
     * @param $ability
     * @return bool|void
     */
    public function before(User $user, $ability)
    {
        if ($user->isAdmin()) {
            return true;
        }
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\Models\User  $user
     * @param  \App\Models\Transaction  $transaction
     * @return \Illuminate\Auth\Access\Response|bool
     */
    public function view(User $user, Transaction $transaction)
    {
        return $this->isBuyer($user, $transaction) || $this->isSeller($user, $transaction);
    }

    /**
     * @param User $user
     * @param Transaction $transaction
     * @return bool
     */
    private function isBuyer(User $user, Transaction $transaction): bool
    {
        return $user->id === $transaction->buyer_id;
    }

    /**
     * @param User $user
     * @param Transaction $transaction
     * @return bool
     */
    private function isSeller(User $user, Transaction $transaction): bool
    {
        return $user->id === $transaction->product->seller->id;
    }
}
