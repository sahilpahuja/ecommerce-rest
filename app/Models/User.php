<?php

namespace App\Models;

use App\Mail\UserCreated;
use App\Mail\UserMailChange;
use App\Transformers\UserTransformer;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes, HasApiTokens;

    const VERIFIED_USER = 1;
    const UNVERIFIED_USER = 0;

    const ADMIN_USER = 1;
    const REGULAR_USER = 0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'verified',
        'verification_token',
        'admin'
    ];
    public string $transformer = UserTransformer::class;

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'verification_token',
    ];

    protected static function boot()
    {
        parent::boot();

        self::created(function (User $user) {
            retry(5, function () use ($user) {
                Mail::to($user)->send(new UserCreated($user));
            }, 100);
        });

        // This event will be called before database commit.
        self::updated(function (User $user) {
            if ($user->isDirty('email')) {
                retry(5, function () use ($user) {
                    Mail::to($user)->send(new UserMailChange($user));
                }, 500);
            }
        });
    }

    /**
     * @return bool
     */
    public function isVerified(): bool
    {
        return $this->verified == self::VERIFIED_USER;
    }

    /**
     * @return bool
     */
    public function isAdmin(): bool
    {
        return $this->admin == self::ADMIN_USER;
    }

    /**
     * @return string
     */
    public static function generateVerificationCode(): string
    {
        return Str::random(40);
    }

    /* Accessors */
    /**
     * @return string
     */
    public function getNameAttribute(): string
    {
        return ucwords($this->attributes['name']);
    }
    /* Mutators */
    /**
     * @param string $name
     */
    public function setNameAttribute(string $name)
    {
        $this->attributes['name'] = strtolower($name);
    }

    /**
     * @param string $email
     */
    public function setEmailAttribute(string $email)
    {
        $this->attributes['email'] = strtolower($email);
    }
}
