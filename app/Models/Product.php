<?php

namespace App\Models;

use App\Transformers\ProductTransformer;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
    use HasFactory, SoftDeletes;

    const AVAILABLE_PRODUCT = 1;
    const UNAVAILABLE_PRODUCT = 2;

    public string $transformer = ProductTransformer::class;
    protected $fillable = [
        'name',
        'description',
        'quantity',
        'status', // available, unavailable
        'image',
        'seller_id'
    ];

    protected $hidden = [
        'pivot'
    ];

    protected static function boot()
    {
        parent::boot();
        self::updated(function (Product $product) {
            if ($product->quantity == 0 && $product->isAvailable()) {
                $product->status = self::UNAVAILABLE_PRODUCT;
                $product->save();
            }
        });
    }


    /**
     * @return bool
     */
    public function isAvailable(): bool
    {
        return $this->status == Product::AVAILABLE_PRODUCT;
    }

    /**
     * @return BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(Category::class);
    }

    /**
     * @return BelongsTo
     */
    public function seller(): BelongsTo
    {
        return $this->belongsTo(Seller::class);
    }

    /**
     * @return HasMany
     */
    public function transactions(): HasMany
    {
        return $this->hasMany(Transaction::class);
    }
}
