<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

trait ResponseHelper
{
    /**
     * @param mixed $data
     * @param int $code
     * @return JsonResponse
     */
    private function successResponse(mixed $data, int $code): JsonResponse
    {
        $this->cacheResponse($data);
        return response()->json($data, $code);
    }

    /**
     * @param mixed $message
     * @param int $code
     * @return JsonResponse
     */
    protected function reportMultipleErrors(mixed $message, int $code): JsonResponse
    {
        return response()->json(['error' => $message, $code], $code);
    }
    /**
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    protected function errorResponse(string $message, int $code): JsonResponse
    {
        return response()->json(['error' => $message, $code], $code);
    }

    /**
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    protected function error(string $message, int $code): JsonResponse
    {
        return $this->successResponse(['error' => $message], $code);
    }
    /**
     * @param Collection $collection
     * @param int $code
     * @return JsonResponse
     */
    protected function showAll(Collection $collection, int $code = 200): JsonResponse
    {
        if ($collection->isEmpty()) {
            return $this->successResponse(['count' => 0, 'data' => $collection], $code);
        }
        $transformer = $collection->first()->transformer;
        $collection = $this->sort($collection, $transformer);
        $collection = $this->filter($collection, $transformer);

        $collection = $this->paginate($collection);
        $transformedCollection = $this->transformData($collection, $transformer);
        $transformedCollection['count'] = $collection->count();
        return $this->successResponse($transformedCollection, $code);
    }
    /**
     * @param Model $model
     * @param int $code
     * @return JsonResponse
     */
    protected function showOne(Model $model, int $code = 200): JsonResponse
    {
        $transformer = $model->transformer;
        $transformedData = $this->transformData($model, $transformer);
        return $this->successResponse($transformedData, $code);
    }

    /**
     * @param string $message
     * @param int $code
     * @return JsonResponse
     */
    protected function showMessage(string $message, int $code = 200): JsonResponse
    {
        return $this->successResponse(['data' => $message], $code);
    }

    protected function transformData($data, string $transformer)
    {
        $transformedData = fractal($data, new $transformer);
        return $transformedData->toArray();
    }

    private function sort(Collection $collection, string $transformer)
    {
        if (request()->has('sort_by')) {
            $transformedAttribute = request()->sort_by;
            $sortByAttribute = $transformer::getOriginalAttribute($transformedAttribute);
            $collection = $collection->sortBy($sortByAttribute);
        }

        return $collection;
    }

    private function filter(Collection $collection, string $transformer)
    {
        foreach (request()->query() as $filteredBy => $value) {
            if ($this->isFilterableAttribute($filteredBy)) {
                $actualAttribute = $transformer::getOriginalAttribute($filteredBy);
                if (isset($actualAttribute, $value)) {
                    $collection = $collection->where($actualAttribute, $value);
                }
            }
        }
        return $collection;
    }

    private function isFilterableAttribute(string $attribute): bool
    {
        return ! in_array($attribute, ['sort_by', 'per_page']);
    }

    /**
     * @param Collection $collection
     * @return LengthAwarePaginator
     */
    private function paginate(Collection $collection): LengthAwarePaginator
    {
        $rules = [
            'per_page' => 'integer|min:10|max:100'
        ];

        Validator::validate(request()->all(), $rules);

        $page = LengthAwarePaginator::resolveCurrentPage();
        $elementsPerPage = 15;

        if (request()->has('per_page')) {
            $elementsPerPage = (int)request()->per_page;
        }
        $results = $collection->slice($elementsPerPage * ($page-1), $elementsPerPage);
        $paginator = new LengthAwarePaginator($results, $collection->count(), $elementsPerPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
        $paginator->appends(request()->all());
        return $paginator;
    }

    private function cacheResponse(mixed $data)
    {
        $url = request()->url();
        $queryParameters = request()->query();
        $method = request()->getMethod();

        ksort($queryParameters);

        $queryString = http_build_query($queryParameters);

        $fullUrl = "$method:{$url}?{$queryString}";

        return Cache::remember($fullUrl, 40, function () use ($data) {
            return $data;
        });
    }
}
