<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class ApplicationSignatureMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * middleware call	next middleware and so on and the flow is passed to the controller
    and controllers returns back the response
    That response can be tweaked by the middleware after the response is sent by controller and tweaking that response/header of response afterwards in middleware is known as AfterMiddleware
     */
    public function handle(Request $request, Closure $next, string $headers = 'X-Name')
    {
        $response = $next($request);
        $response->headers->set($headers, config('app.name'));
        return $response;
    }
}
