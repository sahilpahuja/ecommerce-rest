@component('mail::message')
# Hello {{ $user->name }}

Thank you for registering with us! Verify your account using the link below. If you have any problems in verification you can contact us on 'admin@domain.com'.

@component('mail::button', ['url' => route('users.verify', $user->verification_token)])
Verify Account
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
